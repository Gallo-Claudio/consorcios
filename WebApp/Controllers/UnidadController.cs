﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Servicios;

namespace WebApp.Controllers
{
    public class UnidadController : Controller
    {
        // GET: Unidad
        public ActionResult ListarUnidad(int id)
        {
            List<Entidades.Unidad> listadoUnidades = UnidadServicio.ListarUnidades(id);
            return View(listadoUnidades);
        }
    }
}