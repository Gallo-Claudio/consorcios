﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Servicios;

namespace WebApp.Controllers
{
    public class GastoController : Controller
    {
        // GET: Gasto
        public ActionResult ListarGasto(int id)
        {
            List<Entidades.Gasto> listadoGasto = GastoServicio.ListarGasto(id);
            return View(listadoGasto);
        }
    }
}