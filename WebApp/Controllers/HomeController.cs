﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using Entidades;
using Servicios;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        //DateTime fecha = DateTime.Today  Guarda la fecha de hoy

        // GET: Inicio
        public ActionResult Inicio()
        {
            Hardcodeo.HardcodeoDatos();
            return RedirectToAction("ListarConsorcio","Consorcio");
        }
    }
}