﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;


namespace Harcodeo
{
    public static class Class1
    {
        // Harcodeo todas las entidades

        public static List<Consorcio> consorcio = new List<Consorcio>();

        public static List<Consorcio> Hardcodeo()
        {
            Consorcio con1 = new Consorcio();
            con1.IdConsorcio = 1;
            con1.Nombre = "Edificio Godoy Cruz";
            //con1.IdProvincia = 2;
            con1.Ciudad = "CABA";
            con1.Calle = "Godoy Cruz";
            con1.Altura = 2369;
            con1.DiaVencimientoExpensas = 6;
            con1.FechaCreacion = new DateTime(2020, 09, 29, 22, 50, 00);
            //con1.IdUsuarioCreador = null;

            Consorcio con2 = new Consorcio();
            con2.IdConsorcio = 2;
            con2.Nombre = "Edificio Arieta";
            //con2.IdProvincia = 1;
            con2.Ciudad = "San Justo";
            con2.Calle = "Arieta";
            con2.Altura = 2748;
            con2.DiaVencimientoExpensas = 12;
            con2.FechaCreacion = new DateTime(2020, 09, 29, 22, 50, 48);
            //con2.IdUsuarioCreador = null;

            Consorcio con3 = new Consorcio();
            con3.IdConsorcio = 3;
            con3.Nombre = "Edificio Alberdi";
            //con3.IdProvincia = 2;
            con3.Ciudad = "CABA";
            con3.Calle = "Alberdi";
            con3.Altura = 2387;
            con3.DiaVencimientoExpensas = 1;
            con3.FechaCreacion = new DateTime(2020, 09, 29, 22, 51, 37);
            con3.IdUsuarioCreador = null;

            Consorcio con4 = new Consorcio();
            con4.IdConsorcio = 4;
            con4.Nombre = "Torres Florenci";
            //con4.IdProvincia = 1;
            con4.Ciudad = "Ramos Mejia";
            con4.Calle = "Dr.Gabriel Ardoino";
            con4.Altura = 364;
            con4.DiaVencimientoExpensas = 5;
            con4.FechaCreacion = new DateTime(2020, 09, 29, 22, 51, 56);
            //con4.IdUsuarioCreador = null;

            Consorcio con5 = new Consorcio();
            con5.IdConsorcio = 5;
            con5.Nombre = "Vilanova";
            //con5.IdProvincia = 1;
            con5.Ciudad = "Ramos Mejia";
            con5.Calle = "Tacuari";
            con5.Altura = 620;
            con5.DiaVencimientoExpensas = 21;
            con5.FechaCreacion = new DateTime(2020, 09, 29, 22, 53, 31);
            //con5.IdUsuarioCreador = null;

            Consorcio con6 = new Consorcio();
            con6.IdConsorcio = 6;
            con6.Nombre = "Altos de Gandara";
            //con6.IdProvincia = 1;
            con6.Ciudad = "Haedo";
            con6.Calle = "Juez de la Gandara";
            con6.Altura = 851;
            con6.DiaVencimientoExpensas = 2;
            con6.FechaCreacion = new DateTime(2020, 09, 29, 22, 58, 32);
            //con6.IdUsuarioCreador = null;

            consorcio.Add(con1);
            consorcio.Add(con2);
            consorcio.Add(con3);
            consorcio.Add(con4);
            consorcio.Add(con5);
            consorcio.Add(con6);

            return consorcio;
        }

        //List<Gasto> gasto = new List<Gasto>();


        //List<Provincia> provincia = new List<Provincia>();


        //List<TipoGasto> tipoGasto = new List<TipoGasto>();


        //List<Unidad> unidad = new List<Unidad>();


        //List<Usuario> usuario = new List<Usuario>();
    }
}
