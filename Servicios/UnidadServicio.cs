﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Servicios
{
    public class UnidadServicio
    {
        public static List<Unidad> ListarUnidades(int id)
        {
            List<Unidad> listadoUnidades = Hardcodeo.unidad.FindAll(r => r.IdConsorcio.IdConsorcio == id);
            return listadoUnidades;
        }
        //public static List<Unidad> ListarUnidades()
        //{
        //    return ConsorcioServicio.unidad;
        //}
    }
}
