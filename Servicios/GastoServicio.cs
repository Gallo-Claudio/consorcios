﻿using Entidades;
using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class GastoServicio
    {
        public static List<Gasto> ListarGasto(int id)
        {
            List<Gasto> listadoGasto = Hardcodeo.gasto.FindAll(r => r.IdConsorcio.IdConsorcio == id);
            return listadoGasto;
        }
    }
}
